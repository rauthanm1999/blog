import React,{useState,useEffect} from 'react'
import './App.css';
import Nav from "./components/Nav";
import Bottom from "./components/Bottom";
import Blog from "./components/Blog";
import Section1 from "./components/Section1";
import Section2 from "./components/Section2";
import axios from "axios";
import Pagination from "./components/Pagination";
function App() {
  const [blogs,setBlogs]=useState([])
const [currentPage,setCurrentPage]=useState(1);
const [postsperpage,setPostsperPage]=useState(5)

useEffect(()=>{
  // const axios = require('axios')
  const fetchBlog=async()=>{
    const res=await axios.get('https://jsonplaceholder.typicode.com/posts')
  setBlogs(res.data);
  }
  fetchBlog()
},[]
)
// console.log(blogs)
const indexOfLastPosts=currentPage*postsperpage;
const indexOfFirstPosts=indexOfLastPosts-postsperpage;
const currentPost=blogs.slice(indexOfFirstPosts,indexOfLastPosts);
const paginate=pageNumber=>setCurrentPage(pageNumber)
  return (
    <div className="App">
     <Nav/>
     <Section1/>
     <Blog blogs={currentPost}/>
  <Pagination postsperpage={postsperpage} 

  totalposts={blogs.length}
   paginate={paginate}/>
     <Section2/>
     <Bottom/>
    </div>
  );
}

export default App;
