import React from 'react'

export default function Pagination({postsperpage,totalposts,paginate}) {
    const pageNumbers=[];
    for(let i=0;i<=Math.ceil(totalposts/postsperpage);i++){
        pageNumbers.push(i)
    }
    return (
        <div class="blog-pagination">
        <ul>
            {pageNumbers.map(number=><li 
             key={number}><a 
             onClick={()=>paginate(number)} href="#">{number}</a></li>)}
        </ul>
      </div>
    )
}
