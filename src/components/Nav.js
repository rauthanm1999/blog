import React from 'react'
import logo  from "../components/blog-logo.png";

export default function Nav() {
    return (
        <div className="container">
         
<nav class="navbar navbar-expand-lg">
    <a class="navbar-brand" href="#">
        <img src={logo}/>
    </a>
  <ul class="navbar-nav">
    <li class="nav-item">
      <a class="nav-link" href="#">Blogging Tools</a>
    </li>
    <li class="nav-item">
      <a class="nav-link" href="#">Google</a>
    </li>
    <li class="nav-item">
      <a class="nav-link" href="#">Internet Marketing</a>
    </li>
    <li class="nav-item dropdown">
      <a class="nav-link dropdown-toggle" href="#" id="navbardrop" data-toggle="dropdown">
      Our Services
      </a>
      <div class="dropdown-menu">
        <a class="dropdown-item" href="#">Web Design</a>
        <a class="dropdown-item" href="#">Web Development</a>
        <a class="dropdown-item" href="#">Graphic Design </a>
      </div>
    </li>
    <li class="nav-item">
      <a class="nav-link" href="#">Advertise With Us</a>
    </li>
    <li class="nav-item">
      <a class="request-btn btn" href="#">Request a Free quote</a>
    </li>
    <li class="nav-item">
      <a class="contact-btn btn" href="#">Contact Us</a>
    </li>
  </ul>
  
 
</nav>   
        </div>
    )
}
