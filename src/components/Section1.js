import React from 'react'

export default function Section1() {
    return (
        <div className="newsletter-sec">
           <h1>Smart marketing
               <br/>
starts here</h1> 
<h4>Join over 150,000 marketing managers who get our best digital marketing insights, strategies and tips delivered straight to their inbox.</h4>
<div class="service-form-box">
            <div class="form-group">
              <input type="email" name="" class="form-control" placeholder="ENTER YOUR WORK EMAIL"/>
            </div>
            <div class="submit-btn">
              <button type="submit" class="btn arrow-btn red-btn">Subscribe</button>
            </div>
          </div>
        </div>
    )
}
